import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';

const UpdateProduct = () => {
  const { id } = useParams();
  const [product, setProduct] = useState({
    productName: '',
    productDescription: '',
    quantity: 0,
    price: 0,
  });

  useEffect(() => {
    // Fetch the product details for the specified ID
    axios
      .get(`http://127.0.0.1:4000/admin/products/${id}`)
      .then((response) => {
        setProduct(response.data.data[0]); // Assuming 'data' is an array with a single product object
      })
      .catch((error) => {
        console.error('Error fetching product details:', error);
      });
  }, [id]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setProduct((prevProduct) => ({
      ...prevProduct,
      [name]: value,
    }));
  };

  const handleUpdate = () => {
    // Call the API to update the product
    axios
      .post(`http://127.0.0.1:4000/updateproduct/${product._id}`, product)
      .then((response) => {
        console.log('Product updated successfully:', response.data);
        // Redirect or show a success message
      })
      .catch((error) => {
        console.error('Error updating product:', error);
        // Show an error message or toast notification
      });
  };

  return (
    <div className="container mt-5">
      <h2>Update Product</h2>
      <form>
        <div className="mb-3">
          <label className="form-label">Product Name</label>
          <input
            type="text"
            className="form-control"
            name="productName"
            value={product.productName}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Product Description</label>
          <input
            type="text"
            className="form-control"
            name="productDescription"
            value={product.productDescription}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Quantity</label>
          <input
            type="number"
            className="form-control"
            name="quantity"
            value={product.quantity}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Price</label>
          <input
            type="number"
            className="form-control"
            name="price"
            value={product.price}
            onChange={handleInputChange}
          />
        </div>
        <button type="button" className="btn btn-primary" onClick={handleUpdate}>
          Update Product
        </button>
      </form>
    </div>
  );
};

export default UpdateProduct;



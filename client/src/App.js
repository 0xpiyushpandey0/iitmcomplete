import { Route, Routes, Navigate } from "react-router-dom";
import Main from "./components/Main";
import Signup from "./components/Singup";
import Login from "./components/Login";
import AddProduct from "./components/products/addproduct";
import ProductList from "./components/products/getlistproduct";
import UpdateProduct from "./components/products/updateproduct";

function App() {
	const user = localStorage.getItem("token");

	return (
		<Routes>
			{user && <Route path="/" exact element={<Main />} />}ProductList
			<Route path="/addproduct" exact element={<AddProduct />} />
			<Route path="/productlist" exact element={<ProductList />} />
			<Route path="/update/:id" exact element={<UpdateProduct />} />



			<Route path="/signup" exact element={<Signup />} />
			<Route path="/login" exact element={<Login />} />
			<Route path="/" element={<Navigate replace to="/login" />} />
		</Routes>
	);
}

export default App;
